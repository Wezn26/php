<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>TEST</title>
  </head>
  <body>
    <a href="cookie.php">COOKIE</a>
    <?php
    $count = $_SESSION['count'] ?? 0;
    $count++;
    $_SESSION['count'] = $count;
     ?>
     <p>Счётчик: <?php echo $count; ?></p>
     <!-- <p><?php $id = session_id(); echo $id; ?></p> -->
  </body>
</html>
