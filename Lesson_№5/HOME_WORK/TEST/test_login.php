<?php
require __DIR__ . '/functions/functions.php';

if (empty($_POST['login']) && empty($_POST['password'])) {
  header('Location: form.html');
  exit;
}
$login = $_POST['login'];
$password = $_POST['password'];

if (!checkPassword($login, $password) && !existUser($login)) {
  header('Location: form.html');
  exit;
}

login($login);
getCurrentUser();
header('Location: index.php');
exit;
 ?>
