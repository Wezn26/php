<?php
require __DIR__ . '/functions/functions.php';
require __DIR__ . '/functions/upload.php';

if (!getCurrentUser()) {
  header('Location: form.html');
  exit;
}
?>

<!DOCTYPE html>
<html lang="ru" dir="ltr">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>TEST_HOME_WORK</title>
  </head>
  <body>
    <h1>Добро пожаловать <?php echo getCurrentUser(); ?>!!!</h1>
    <a href="exit.php" style="cursor: pointer";>Выход с сайта</a>

    <form action="script.php" method="POST" enctype="multipart/form-data">
      <fieldset>
        <legend>Загрузка файла</legend>
        <p>
          <p>Название картинки:</p><br>
          <input type="text" name="my_image" placeholder="Введите название картинки">
        </p>

        <p>
          <p>Загрузка файла:</p><br>
          <input type="file" name="my_file">
        </p>
      </fieldset>
    </form>
    <hr>
    <h2>История</h2>
    <table>
      <tr>
        <th>Дата</th>
        <th>Пользователь</th>
        <th>Подпись</th>
        <th>Картинка</th>        
      </tr>
      <?php foreach (getRecords() as $record): ?>
        <td><?php echo $record[1]; ?></td>
        <td><?php echo $record[2]; ?></td>
        <td><?php echo $record[3]; ?></td>
        <td><img src="/HOME_WORK/<?php echo $record[3]; ?>" width="100px"> </td>
      <?php endforeach ?>  
    </table>
  </body>
</html>
