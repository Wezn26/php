<?php
require __DIR__ . '/functions/upload.php';
require __DIR__ . '/functions/functions.php';

if (!empty($_POST['my_image']) && !empty($_POST['my_file'])) {
  $data = [];
  $data['date'] = date('d.m.Y');
  $data['name'] = getCurrentUser();
  $data['title'] = strip_tags($_POST['my_image']);
  $res = fileUpload('my_file');
  if (false != $res) {
    $data['pathFile'] = (string)$res;
  }

  $getAllLine = readRecords();
  if (isset($data['date']) && isset($data['name'])
      && isset($data['title']) && isset($data['pathFile'])) {
    $line = implode('|', $data);
    $getAllLine[] = (string)$line;
    file_put_contents(__DIR__ . '/db/data.txt', implode("\n", $getAllLine));
  }
}

header('Location: index.php');
exit;
 ?>
