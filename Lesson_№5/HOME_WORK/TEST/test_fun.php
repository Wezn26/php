<!-- FUNCTIONS PHP START -->
<?php
function getUsersList()
{
  $users = [
    'Petr'=>'qwerty',
    'Ivan'=>'123456'
  ];
  $usersList = [];
  foreach ($users as $login => $pass) {
    $usersList[$login] = password_hash($pass, PASSWORD_DEFAULT);
  }
  return $usersList;
}
//var_dump(getUsersList());

function existUser($login)
{
  $user = getUsersList();
  return isset($user[$login]);
}
assert(true === existUser('Ivan'));
assert(false === existUser('Tanya'));

function checkPassword($login, $password)
{
  $users = getUsersList();
  return isset($users[$login]) && password_verify($password, $users[$login]);
}
//var_dump(checkPassword('Petr', 'qwerty'));
assert(true === checkPassword('Petr', 'qwerty'));
assert(false === checkPassword('Liza', 'love'));

function login($login)
{
  setcookie('user', $login, time()+3600);
}
login('Ivan');
//var_dump($_COOKIE['user'] === 'Ivan');

function getCurrentUser()
{
  if (isset($_COOKIE['user'])) {
    return $_COOKIE['user'];
  } else {
    return null;
  }
}
//var_dump(getCurrentUser());
//assert(getCurrentUser() === 'Ivan');

function logout()
{
  unset($_COOKIE['user']);
  setcookie('user', '', time()-3600);
}
 ?>

<!-- FUNCTIONS PHP END -->
