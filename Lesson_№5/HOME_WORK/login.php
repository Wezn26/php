<?php
require __DIR__ . '/functions/functions.php'; //подключаем функцию


if (empty($_POST['login']) && empty($_POST['password'])) {
  header('Location: form.html');
  exit;
}

$login = $_POST['login'];
$password = $_POST['password'];

/*
1. ЕСЛИ пользователь уже вошел , ТО редирект на главную страницу
2. ЕСЛИ пользователь не вошел - отображает форму входа
3. ЕСЛИ введены данные в форму входа - проверяем их и ЕСЛИ проверка прошла, ТО запоминаем информацию о вошедшем пользователе
 */
//
if (!checkPassword($login, $password)) {
    if (!existUser($login)) {
      header('Location: form.html');
      exit;
    }
}

login($login);
getCurrentUser();
 header('Location: index.php');
 exit;

 ?>
