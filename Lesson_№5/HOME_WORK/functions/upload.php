<?php
function checkTypeFile($mimeType)
{
  $mediaTypes = ['image/gif', 'image/svg+xml', 'image/jpeg', 'image/png'];
  return in_array($mimeType, $mediaTypes);
}

function fileUpload($field)
{
  $file = $_FILES[$field];
  if (empty($file) && 0!= $file['error'] && !checkTypeFile($file['type'])) {
    return false;
  }

  if (is_uploaded_file($file['tmp_name'])) {
    $res = move_uploaded_file($file['tmp_name'], __DIR__ . '/../images/' . $file['name']);
    if ($res) {
      return '/images/' . $file['name'];
    } else {
      return false;
    }
  }
  return false;
}

function getPicture()
{
  $scanDir = scandir(__DIR__.'/../images');
  return array_diff($scanDir, ['.', '..']);
}

function readRecords()
{
  return file(__DIR__.'/../db/data.txt', FILE_IGNORE_NEW_LINES);
}

function getRecords()
{
  $data = [];
  $records = readRecords();//читаем data.txt
  foreach ($records as $str) {
    $data[] = explode('|', $str);
  }
  return $data;

}

 ?>
