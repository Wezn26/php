<?php
function getUsersList()//Возвращает массив всех пользователей и хешей их паролей
{
  $users = [
    'Petr'=>'qwerty',
    'Ivan'=>'123456'
  ];
  $usersList = [];
  foreach ($users as $login => $pass) {
    $usersList[$login] = password_hash($pass, PASSWORD_DEFAULT);
  }
  return $usersList;
}
//var_dump(getUsersList())

function existsUser($login)//Проверяет существует ли пользователь с заданным логином?
{
  $user = getUsersList();
  return isset($user[$login]);
}

//assert(true === existUser('Petr'));
//assert(false === existUser('Lev'));

function checkPassword($login, $password) //Возвращает true тогда, когда существует пользователь с указанным логином и введёныый им пвроль прошёл проверку
{
  $users = getUsersList();
  return isset($users[$login]) && password_verify($password, $users[$login]);
}
//var_dump(checkPassword('Ivan', '123456'));
//assert(true === checkPassword('Ivan', '123456'));
//assert(false === checkPassword('Katya', 'love'))

function login($login)//Устанавливаем куку
{
  setcookie('user', $login, time()+3600);
}

function getCurrentUser()// Возвращает либо имя вошедшего на сайт, либо null
{
  if (isset($_COOKIE['user'])) {
    return $_COOKIE['user'];
  } else {
    return null;
  }
}

function logout()//Выход с сайта
{
  unset($_COOKIE['user']);
  setcookie('user', '', time()-3600);
}



 ?>
