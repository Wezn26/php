<?php $images =  include __DIR__ . '/data.php'; ?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>FOTO GALLERY</title>
    <style>
      div.foto {
        margin: 100px;
      }

       div.foto img {
         width: 100px;
         border: 2px solid red;
       }
    </style>
  </head>
  <body>
   <h1>ФОТОГАЛЛЕРЕЯ</h1>
   <?php foreach ($images as $num => $image) { ?>
     <div class="foto">
       <a href="image.php?file=<?php echo $num ?>">
           <img src="./img/<?php echo $image ?> " alt="">
       </a>
     </div>
  <?php } ?>

   <!-- <div class="foto">
     <img src="./img/2.jpg" alt="image">
   </div>
   <div class="foto">
     <img src="./img/3.jpg" alt="image">
   </div> -->
  </body>
</html>
