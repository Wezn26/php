<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>WHILE</title>
  </head>
  <body>
<?php
   // $i = 1;
   // while ($i <= 10) {
   //   echo $i;
   //   $i++;
   // }
 ?>

 <?php
 // $a = 0;
 //  do {
 //    echo $a;
 //    $a++;
 //  } while ($a <= 10);
  ?>


  <?php
   // $path = '/var/www/html/php/text.txt';
   // $fh = fopen($path, 'r');
   //
   // $line = fgets($fh);
   // echo $line;
   //
   // $line = fgets($fh);
   // echo $line;
   //
   // $line = fgets($fh);
   // echo $line;
   //
   // $line = fgets($fh);
   // echo $line;
   //
   // fclose($fh);
   ?>

   <?php
// echo __DIR__;
// die;

   // $path = __DIR__ . '/text.txt';
   // $fh = fopen($path, 'r');
# 1 ЧТЕНИЕ ФАЙЛОВ
   // while (!feof($fh)) {
   //   $line = fgets($fh);
   //   echo $line;
   // }
   //
   // fclose($fh);

# 2
  // while (false !== ($line = fgets($fh))) {
  //   echo $line;
  // }
  //
  //  fclose($fh);

# 3
// $lines = file($path, FILE_IGNORE_NEW_LINES);
// var_dump($lines);

# 4
// $content = file_get_contents($path);
// var_dump($content);

# 5
// var_dump(file_exists($path));

# 6
// var_dump(is_readable($path));

# 7
// readfile($path);
    ?>

<?php
# ЗАПИСЬ В ФАЙЛ
// $path = __DIR__ . '/text.txt';
# 1
// $fh = fopen($path, 'w');
// fwrite($fh, 'Hellow World!!!');
// fclose($fh);

# 2
// var_dump(file_exists($path));
// file_put_contents($path, 'New Age!!!');

 ?>

 <?php
 # 1
  // $list = scandir(__DIR__) ;
  // var_dump($list);

# 2
// $list2 = pathinfo(__DIR__ .  '/text.txt')  ;
// var_dump($list2);

# 3
// $list3 = dirname(__DIR__ . '/text.txt');
// var_dump($list3);

# 4
// $list4 = filesize(__DIR__ . '/text.txt');
// var_dump($list4);

# 5
// $list5 = realpath(__DIR__ . '/text.txt');
// var_dump($list5);
  ?>

  <form class="" action="upload.php" method="post" enctype="multipart/form-data">
       <input type="file" name="picture">
       <button type="submit" name="button">Отправить</button>
  </form>
  </body>
</html>
