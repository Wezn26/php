<!-- FUNCTION START -->
<?php
  function calc($x, $y, $sign)
  {
    switch ($sign) {
      case '+':
        return $x + $y;
        break;
      case '-':
        return $x - $y;
        break;
      case '*':
        return $x * $y;
        break;
      case '/':
        return $x / $y;
        break;

      default:
        return null;
        break;
    }
  }
  assert(4 == calc(2, 2, '+'));
  assert(4 == calc(6, 2, '-'));
  assert(4 == calc(2, 2, '*'));
  assert(4 == calc(8, 2, '/'));
 ?>
<!-- FUNCTION END -->

<!-- VERIFICATION START -->
<?php
  if (isset($_POST['x']) && isset($_POST['y'])) {
    $x = (int)$_POST['x'];
    $y = (int)$_POST['y'];
  } else {
    $x = null;
    $y = null;
  }
  $oper = ['+', '-', '*', '/'];
  if (isset($_POST['sign']) && in_array($_POST['sign'], $oper)) {
    $sign = $_POST['sign'];
  } else {
    $sign = null;
  }
  $res = calc($x, $y, $sign);
 ?>
<!-- VERIFICATION END -->
<!DOCTYPE html>
<html lang="ru" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>CALCULATOR</title>
  </head>
  <body>
    <form action="test_calc.php" method="post" style="margin: 10em 0 0 10em;">
      <input type="number" name="x" value="<?php echo $x; ?>">
      <select name="sign">
        <option value="+"<?php if ('+' == $sign) { ?> selected <?php } ?>>+</option>
        <option value="-"<?php if ('-' == $sign) { ?> selected <?php } ?>>-</option>
        <option value="*"<?php if ('*' == $sign) { ?> selected <?php } ?>>*</option>
        <option value="/"<?php if ('/' == $sign) { ?> selected <?php } ?>>/</option>
      </select>
      <input type="number" name="y" value="<?php echo $y; ?>">
      <button type="submit" name="button" style="cursor: pointer;">=</button>
      <h2 style="display: inline-block; margin-left: 1em; vertical-align: middle; "><?php echo $res ?></h2>
    </form>
  </body>
</html>
