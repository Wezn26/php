<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Домашнее задание ко второму уроку</title>
  </head>
  <body>
    <h1>Напишите программу, которая составит и выведет в браузер таблицу истинности для логических операторов &&, || и xor.</h1>

    <table border="1" style="border-collapse: collapse">
      <tr>
        <td>a</td>
        <td>b</td>
        <td>&&</td>
        <td>||</td>
        <td>xor</td>
      </tr>

      <tr>
        <td><?php $a = false; echo (int)$a; ?></td>
        <td><?php $b = false; echo (int)$b; ?></td>
        <td><?php  echo (int)($a && $b); ?></td>
        <td><?php  echo (int)($a || $b); ?></td>
        <td><?php  echo (int)($a xor $b); ?></td>
      </tr>

      <tr>
        <td><?php $a = true; echo (int)$a; ?></td>
        <td><?php $b = false; echo (int)$b; ?></td>
        <td><?php  echo (int)($a && $b); ?></td>
        <td><?php  echo (int)($a || $b); ?></td>
        <td><?php  echo (int)($a xor $b); ?></td>
      </tr>

      <tr>
        <td><?php $a = false; echo (int)$a; ?></td>
        <td><?php $b = true; echo (int)$b; ?></td>
        <td><?php  echo (int)($a && $b); ?></td>
        <td><?php  echo (int)($a || $b); ?></td>
        <td><?php  echo (int)($a xor $b); ?></td>
      </tr>

      <tr>
        <td><?php $a = true; echo (int)$a; ?></td>
        <td><?php $b = true; echo (int)$b; ?></td>
        <td><?php  echo (int)($a && $b); ?></td>
        <td><?php  echo (int)($a || $b); ?></td>
        <td><?php  echo (int)($a xor $b); ?></td>
      </tr>

    </table>


    <?php
      function gender($name)
      {
        mb_strtolower($name);

        if ($name == 'женя' || $name == 'саша') {
          return null;
        }

        // набор гласных букв характерных для женских имен
        $femaleletters = 'аяи';
        $femaleSearch = (strspn($name, $femaleletters, -1));

        if ($femaleletters == 1) {
          if ($mame == 'паша' || $name == 'миша' || $name == 'витя') {
            return 'm';
          } else {
            return 'f';
          }
        } elseif ($femaleSearch == 0) {
          return 'm';
        }

      }

assert('m' == gender('Иван'));
assert('m' == gender('Сергей'));
assert('m' == gender('Дмитрий'));
assert('f' == gender('Лиза'));
assert('f' == gender('Светлана'));
assert('f' == gender('Анна'));
assert(null == gender('женя'));
assert(null == gender('Саша'));
assert('m' == gender('витя'));
assert('m' == gender('Миша'));

     ?>



  </body>
</html>
