<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Массивы</title>
  </head>
  <body>
  <h2><?php var_dump([18, 24, 36, 'Hi']);  ?></h2>
  <h2><?php $x = [11, 22, 30, 'Hi', 'Bye', true]; $x[] = 100; var_dump($x);  ?></h2>
  <h2><?php $y = [9, 20 => 18, 10 => 27, 33]; var_dump($y)?></h2>
  <h2><?php
    $x = [
      'январь' => 31,
      'февраль' => 29,
      'март' => 31,
    ];
    var_dump($x);
    var_dump($x['февраль']);
   ?></h2>
  <?php
    $x = [
      'январь' => 31,
      'февраль' => 29,
      'март' => 31,
    ];
   ?>
   <table border="1" style="border-collapse: collapse; font-size: 30px; font-weight: bold">
     <?php

     foreach ($x as $key => $value) {
       ?>
       <tr>
         <td><?php echo $key; ?></td>
         <td><?php echo $value; ?></td>
       </tr>
       <?php
     }
     ?>
   </table>

   <h2>
     <?php
        // $arr = [1, 'Hellow', true];
        $str = 'Hellow, Good, Morning';
        $res = explode(',', $str);
        var_dump($res);
        // $res = implode(" , ", $arr);
        // $res = in_array(1, $arr);

        // var_dump($res);
        // echo $res;
      ?>
   </h2>

   <h2><?php
   if (isset($_GET['name'])) {
     $name = $_GET['name'];
   } else {
     $name = null;
   }

   $name = $_GET['name'] ?? 'Неизвестный';
   // var_dump($_GET);
   echo $name;
   ?></h2>


   <form  action="send.php" method="post">
      <p>Имя пользователя: <input type="text" name="user" value=""></p>
      <p>Пароль: <input type="password" name="pass" value=""></p> <br>
      <button type="submit" name="button">ОТПРАВИТЬ</button>
   </form>

  </body>
</html>
