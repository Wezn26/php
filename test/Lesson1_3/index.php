<!-- 01 ВЫРАЖЕНИЯ И ПЕРЕМЕННЫЕ, ТИРЫ -->


<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Изучаем PHP</title>
  </head>
  <body>
    <h1>Привет PHP!!!</h1>
    <h3><?php echo 'Hellow World!!!'; ?></h3>
    <h3><?php echo 4 * 4; ?></h3>
    <h3><?php echo 2 * 5; ?></h3>
    <h3><?php var_dump( 3 / 2); ?></h3>
    <h3><?php var_dump( 6 / 2); ?></h3>

    <h2> <?php var_dump( 1 / 3 ) ?></h2>
    <h2> <?php var_dump( 18 % 4 ) ?></h2>
    <h2> <?php var_dump( '20cats' + 40 ) ?></h2>

    <h2><?php
     echo $a = 2;
     $x = ( $y = 12 ) - 8;
     echo $x;
     ?></h2>

     <h2> <?php var_dump( 1 == 1.0 ) ?></h2>
     <h2> <?php var_dump( 1 === 1.0 ) ?></h2>
     <h2> <?php var_dump( '02' == 2 ) ?></h2>
     <h2> <?php var_dump( '02' === 2 ) ?></h2>
     <h2> <?php var_dump( '02' == '2' ) ?></h2>
     <h2> <?php
      $x = true xor true;
      var_dump( $x )
      ?></h2>
      <h2><?php
       $str1 = 'test';
       $str2 = "test";
       $res = $str1 === $str2;
       var_dump($res);
      ?></h2>

  </body>
</html>
