<!-- УРОК №2 ФУНКЦИИ, ОПЕРАТОРЫ КОНТРОЛЯ. вКЛЮЧЕНИЕ ФАЙЛОВ -->

<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>02 Функции. Операторы контроля. Включение файлов</title>
  </head>
  <body>
    <h2><?php echo 1; ?></h2>
    <h2><?php var_dump( (int)'12'); ?></h2>
    <h2><?php var_dump( (boolean)'0'); ?></h2>
    <?php
       $age = 17;
       if ($age < 18) {
         echo "Вы пришли слишком рано!!!";
       } elseif ($age < 21) {
         echo "Уже скоро!!!";
       }
        else {
         echo "В самый раз!!!";
       }

     ?>

     <h2>
       <?php
        $age = 18;
        $gender = 'female';

        if ($age >= 18 && 'male' == $gender) {
          echo "Берём на работу!!!";
        } else {
          echo "Не в это раз!!!";
        }

        ?>
     </h2>

     <h2>
       <?php
       $color = 'red';

         switch ($color) {
           case 'red':
             echo "Красный свет!!!";
             break;
           case 'yellow':
              echo "Жёлтый свет!!!";
              break;
           case 'green':
              echo "Зелёный свет!!!";
             break;

           default:
             echo "Введите красный, жёлтый, зелёный свет!!!";
             break;
         }

        ?>
     </h2>

     <h2><?php include __DIR__ . '/functions.php'; ?></h2>
     <h2><?php echo maxnum(4,6) ?></h2>

     <h2><?php echo PHP_OS; ?></h2>
     <h2><?php echo PHP_VERSION; ?></h2>
     <h2><?php echo __DIR__; ?></h2>
  </body>
</html>
