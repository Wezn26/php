<!-- PHP FUNCTIONS START -->
<?php
function calcul($x, $y, $sign)
{
   switch ($sign) {
     case '+':
       return $x + $y;
       break;
     case '-':
       return $x - $y;
       break;
     case '*':
       return $x * $y;
       break;
     case '/':
       return $x / $y;
       break;

     default:
        return null;
       break;
   }
}

assert(4 == calcul(2, 2, '+'));
assert(4 == calcul(6, 2, '-'));
assert(4 == calcul(2, 2, '*'));
assert(4 == calcul(8, 2, '/'));
assert(null == calcul(8, 2, '&&'));
 ?>
<!-- PHP FUNCTIONS END -->

<!-- VERIFICATION START -->
<?php
if (isset($_POST['x'])) {
  $x = (int)$_POST['x'];
} else {
  $x = null;
}

if (isset($_POST['y'])) {
  $y = (int)$_POST['y'];
} else {
  $y = null;
}

$oper = ['+', '-', '*', '/'];

if (isset($_POST['sign']) && in_array($_POST['sign'], $oper)) {
  $sign = $_POST['sign'];
} else {
  $sign = null;
}

$res = calcul($x, $y, $sign);
 ?>
<!-- VERIFICATION END -->

<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Калькулятор на PHP</title>
  </head>
  <body>
    <form  action="calculator.php" method="post" style="margin: 10em 0 0 10em;">
      <h1>КАЛЬКУЛЯТОР</h1>
       <input type="number" name="x" value="<?php echo $x ?>">
       <select class="" name="sign" style="font-size: 20px; font-weight: bold; ">
          <option value="+" <?php if ('+' == $sign) {?> selected <?php } ?>>+</option>
          <option value="-" <?php if ('-' == $sign) {?> selected <?php } ?>>-</option>
          <option value="*" <?php if ('*' == $sign) {?> selected <?php } ?>>*</option>
          <option value="/" <?php if ('/' == $sign) {?> selected <?php } ?>>/</option>
       </select>
       <input type="number" name="y" value="<?php echo $y ?>">
       <input type="submit" name="" value="=" style="cursor: pointer; ">

      <h2 style="display: inline-block; margin-left: 1em; vertical-align: middle; "> <?php echo $res; ?></h2>
    </form>
  </body>
</html>
