<?php
function calculator($a, $b, $sign)
{
   switch ($sign) {
     case '+':
       return $a + $b;
       break;
     case '-':
       return $a - $b;
       break;
     case '*':
       return $a * $b;
       break;
     case '/':
       return $a / $b;
       break;

     default:
        return null;
       break;
   }
}

assert(4 == calculator(2, 2, '+'));
assert(4 == calculator(6, 2, '-'));
assert(4 == calculator(2, 2, '*'));
assert(4 == calculator(8, 2, '/'));
assert(null == calculator(8, 2, '&&'));
 ?>
