<?php
require __DIR__ . '/func_calculator.php';





if (isset($_POST['x'])) {
  $x = (int)$_POST['x'];
} else {
  $x = null;
}

if (isset($_POST['y'])) {
  $y = (int)$_POST['y'];
} else {
  $y = null;
}

$oper = ['+', '-', '*', '/'];

if (isset($_POST['sign']) && in_array($_POST['sign'], $oper)) {
  $sign = $_POST['sign'];
} else {
  $sign = null;
}

$res = calculator($x, $y, $sign);
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Домашнее задание№3</title>
  </head>
  <body>
<!-- FIRST CALCULATOR START     -->
    <!-- <?php
    // $x = $_POST['x'];
    // $y = $_POST['y'];


    // $res = $x + $y;
     ?>
<form action="home_work3.php" method="post">
  <input type="number" name="x" style="font-size: 20px;">
  +
  <input type="number" name="y" style="font-size: 20px;">
  <input type="submit"  value="=" style="cursor: pointer;">
  =
  <?php echo $res ?>
</form> -->
<!-- FIRST CALCULATOR END -->

<!-- SECOND CALCULATOR START -->

<form  action="home_work3.php" method="post" style="margin-top: 10em;">
   <input type="number" name="x" value="<?php echo $x ?>">
   <select class="" name="sign" style="font-size: 20px; font-weight: bold; ">
      <option value="+" <?php if ('+' == $sign) {?> selected <?php } ?>>+</option>
      <option value="-" <?php if ('-' == $sign) {?> selected <?php } ?>>-</option>
      <option value="*" <?php if ('*' == $sign) {?> selected <?php } ?>>*</option>
      <option value="/" <?php if ('/' == $sign) {?> selected <?php } ?>>/</option>
   </select>
   <input type="number" name="y" value="<?php echo $y ?>">
   <input type="submit" name="" value="=" style="cursor: pointer; ">

  <h2 style="display: inline-block; margin-left: 1em; vertical-align: middle; "> <?php echo $res; ?></h2>
</form>
<!-- SECOND CALCULATOR END -->

  </body>
</html>
