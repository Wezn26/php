<?php
 function maxnum( $x, $y ) {
 if ($x > $y) {
   return $x;
 } else {
   return $y;
 }
}
 $m = maxnum( -1, 1);
 echo $m;

 function sum($a, $b)
 {
   return $a + $b;
 }
 assert(1 == sum(1, 0));
 assert(1 == sum(0, 1));
 assert(46 == sum(34, 12));
 ?>
