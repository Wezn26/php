<?php 
function myAutoload($className)
{
  $file = __DIR__ . '/../../Classes/' . str_replace('\\', '/', $className) . '.php';
  if (file_exists($file)) {
    require $file;
  }
}
spl_autoload_register('myAutoload');