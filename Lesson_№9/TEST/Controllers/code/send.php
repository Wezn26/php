<?php
require_once __DIR__ . '/../functions/autoload.php';
use \Models\GuestBook\DB;
if (isset($_POST['add']) && isset($_POST['text'])) {
  $text = (string)$_POST['text'];

  $db = new DB;
  $sql = 'INSERT INTO text
         (text)
         VALUES
         (:text)';
  $data = [':text' => $text];
  $query = $db->query($sql, $data);
  if (false === $query) {
    die('Запись не добавлена!!!');
  } else {
    header('Location: ./../../index.php');
  }
}
