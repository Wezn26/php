<?php 
namespace Models\GuestBook;
use PDO as PDO;
class DB
{
    protected $dbh;
    public function __construct()
    {
      $config = require __DIR__ . './../../../Controllers/config/config.php';
      $this->dbh = new PDO($config['dsn'], $config['username'], $config['password']);
    }

    public function query( string $sql, array $data = [])
    {
      $sth = $this->dbh->prepare($sql);
      $result = $sth->execute($data);
      if (false === $result) {
        return false;
      } else {
        $value = $sth->fetchAll();
        return $value;
      }
    }
}