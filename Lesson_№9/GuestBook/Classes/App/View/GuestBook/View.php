<?php
namespace App\View\GuestBook; 
class View
{
  public function display($template)
  {
    include __DIR__ . './../../../../templates/' . $template;
  }
}