<?php
namespace App\Models\GuestBook;
class Record
{
  protected $message;

  public function __construct($message)
  {
    $this->message = $message;
  }

  public function getMessage()
  {
    return $this->message;
  }
}
