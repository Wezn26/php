<?php

require __DIR__ . '/classes/DB.php';
require __DIR__ . '/classes/View.php';


$db = new DB;
$view = new View;
$sql = 'SELECT * FROM news ORDER BY id DESC';
$news = $db->query($sql);
$view->assign('news', $news);
$view->display('news.php');