<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Update</title>
  </head>
  <body>
    <a href="./../parts/article.php?id=<?php echo $this->data['updateArticle']['id']; ?>">Назад</a>
    <h2>Изменить новость:</h2>
    <form action="./../parts/send.php" method="post">
      <input type="hidden" name="id" value="<?php echo $this->data['updateArticle']['id']; ?>">
      <p>Заголовок:</p><br>
      <input type="text" name="title" value="<?php echo $this->data['updateArticle']['headline']; ?>"><br>
      <p>Текст:</p><br>
      <textarea name="text" rows="10" cols="30"><?php echo $this->data['updateArticle']['text']; ?></textarea><br>
      <p>Автор:</p><br>
      <input type="text" name="author" value="<?php echo $this->data['updateArticle']['author']; ?>"><br>
      <input type="submit" name="update" value="Добавить запись">
    </form>
  </body>
</html>
