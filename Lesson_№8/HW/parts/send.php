<?php
require __DIR__ . './../classes/DB.php';
 // Добавить новость

 if (isset($_POST['add'])) {
   if (isset($_POST['title']) && isset($_POST['text']) && isset($_POST['author'])) {
     $title = (string)$_POST['title'];
     $text = (string)$_POST['text'];
     $author = (string)$_POST['author'];

     $db = new DB;

     $sql = 'INSERT INTO news
             (headline, text, author)
             VALUES
             (:headline, :text, :author)';
    $data = [
      ':headline' => $title,
      ':text' => $text,
      ':author' => $author
    ];

    $newNews = $db->execute($sql, $data);

    if (false === $newNews) {
      die('Новость не добавлена!!!');
    } else {
      header('Location: ./../index.php');
      die;
    }
   }
 }
 //УДАЛИТЬ НОВОСТЬ

 if (isset($_POST['delete']) && isset($_POST['id'])) {
   $db = new DB;
   $sql = 'DELETE FROM news WHERE id=:id';
   $data = [':id' => (int)$_POST['id']];

   $removeNews = $db->execute($sql, $data);
   if (false === $removeNews) {
     die('Новость не удалена!!!');
   } else {
     header('Location: ./../index.php');
   }
 }

  //ОБНОВИТЬ НОВОСТЬ

  if (isset($_POST['update']) && isset($_POST['id']) && isset($_POST['title']) && isset($_POST['text']) && isset($_POST['author'])) {
     $id = (int)$_POST['id'];
     $title = (string)$_POST['title'];
     $text =(string)$_POST['text'];
     $author = (string)$_POST['author'];

     $db = new DB;
     $sql = 'UPDATE news SET headline=:headline, text=:text, author=:author WHERE id=:id';
     $data = [
       ':id' => $id,
       ':headline' => $title,
       ':text' => $text,
       ':author' => $author
     ];

     $result = $db->execute($sql, $data);
     if ($result === false) {
       die('Обновить не удалось!!!');
     } else {
       header('Location: article.php?id=' . $id);
       die;
     }
  }
