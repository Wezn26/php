<?php
class View
{
  protected $data = [];
  public function assign($name, $value)
  {
    $this->data[$name] = $value;
  }
  public function display($template)
  {
    include __DIR__ . './../templates/' . $template;
  }
  public function render($template)
  {
    ob_start();
    include __DIR__ . './../templates/' . $template;
    $content = ob_get_contents();
    ob_end_clean();
    return $content;
  }
}
