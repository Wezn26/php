<?php
require __DIR__ . '/Controllers/functions/autoload.php';
use \App\Models\News\DB;
use \App\View\News\View;
$db = new DB;
//var_dump($db);
$view = new View;
$sql = 'SELECT * FROM news ORDER BY id DESC';
$news = $db->query($sql);
$view->assign('news', $news);
$view->display('main_news.php');
