<?php
function myAutoload($className)
{
  //var_dump($className);die;
  $file =  __DIR__ . '/../../Classes/' . str_replace('\\', '/', $className) . '.php';
  if (file_exists($file)) {
    require $file;
  }
}
spl_autoload_register('myAutoload');
