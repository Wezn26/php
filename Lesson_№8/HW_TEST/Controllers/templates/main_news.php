<!DOCTYPE html>
<html lang="ru" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Новости</title>
  </head>
  <body>
    <h1>Главные Новости</h1>
    <hr>
    <?php foreach ($this->data['news'] as $article): ?>
      <article>
        <!-- Заголовок -->
        <h2>
          <a href="./Controllers/code/article.php?id=<?php echo $article['id']; ?>">
            <?php echo $article['headline']; ?>
          </a>
        </h2>
        <!-- Текст -->
        <p><?php echo $article['text']; ?></p>
        <!-- Автор -->
        <p><?php echo $article['author']; ?></p>
      </article>
      <hr>
    <?php endforeach; ?>
    <h2>Добавить Запись</h2>
    <form action="./../code/send.php" method="post">
      <p>Заголовок:</p><br>
      <input type="text" name="title"><br>
      <p>Текст:</p><br>
      <textarea name="text" rows="10" cols="30"></textarea><br>
      <p>Автор:</p><br>
      <input type="text" name="author">
      <input type="submit" name="add" value="Добавить запись">
    </form>
  </body>
</html>
