<?php
require_once __DIR__ . '/../functions/autoload.php';
use \App\Models\News\DB;
use \App\View\News\View;

if (isset($_POST['id'])) {
  $id = (int)$_POST['id'];
}

$db = new DB;

$sql = 'SELECT * FROM news WHERE id=:id;';
$data = [':id' => $id];
$updateArticle = $db->query($sql, $data);
$view = new View;
$view->assign('updateArticle', array_shift($updateArticle));
$view->display('update.php');
