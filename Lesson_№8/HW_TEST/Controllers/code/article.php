<?php
require_once __DIR__ . '/../functions/autoload.php';
use \App\Models\News\DB;
use \App\View\News\View;

if (isset($_GET['id'])) {
  $id = (int)$_GET['id'];
} else {
  die('Новость не найдена!!!');
}

$db = new DB;

$sql = 'SELECT * FROM news WHERE id=:id;';

$data = [
  ':id' => $id
];

$article = $db->query($sql, $data);
$view = new View;
$view->assign('article', array_shift($article));
$view->display('article.php');
