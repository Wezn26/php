<?php
require_once __DIR__ . '/../functions/autoload.php';
use \App\Models\News\DB;

//ADD NEWS
if (isset($_POST['add'])) {
  if (isset($_POST['title']) && isset($_POST['text']) && isset($_POST['author'])) {
    $title = (string)$_POST['title'];
    $text = (string)$_POST['text'];
    $author = (string)$_POST['author'];

    $db = new DB;
    $sql = 'INSERT INTO news
            (headline, text, author)
            VALUES
            (:headline, :text, :author)';
    $data = [
      ':headline' => $title,
      ':text' => $text,
      ':author' => $author
    ];

    $newNews = $db->query($sql, $data);
    if (false === $newNews) {
      die('Новость не добавлена!!!');
    } else {
      header('Location: ./../../index.php');
      die;
    }
  }
}
//DELETE NEWS
if (isset($_POST['delete']) && isset($_POST['id'])) {
  $db = new DB;
  $sql = 'DELETE FROM news WHERE id=:id;';
  $data = [':id' => (int)$_POST['id']];
  $deleteNews = $db->query($sql, $data);

  if (false === $deleteNews) {
    die('Новость не удалена!!!');
  } else {
    header('Location: ./../../index.php');
    die;
  }
}

//UPDATE NEWS
if (isset($_POST['update'])) {
  if (isset($_POST['id']) && isset($_POST['title']) && isset($_POST['text']) && isset($_POST['author'])) {
    $id = (int)$_POST['id'];
    $title = (string)$_POST['title'];
    $text = (string)$_POST['text'];
    $author = (string)$_POST['author'];

    $db = new DB;
    $sql = 'UPDATE news SET headline=:headline, text=:text, author=:author WHERE id=:id;';
    $data = [
      ':id' => $id,
      ':headline' => $title,
      ':text' => $text,
      ':author' => $author
    ];

    $updateNews = $db->query($sql, $data);
    if (false === $updateNews) {
      die('Новость не обновлена!!!');
    } else {
      header('Location: article.php?id=' . $id);
      die;
    }
  }
}
