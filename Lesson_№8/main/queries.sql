CREATE TABLE news(
  id SERIAL,
  headline TEXT,
  text TEXT,
  author VARCHAR(100)
);

INSERT INTO news
  (headline, text, author)
VALUES
  ('Карабах', 'Война приобретает ожесточённый характер', 'Леонид');

  CREATE TABLE persons(
    id SERIAL,
    firstName TEXT,
    lastName TEXT,
    author VARCHAR(100)
  );
  INSERT INTO persons
    (firstName, lastName, author)
  VALUES
   ('Иван', 'Иванов', '42');
