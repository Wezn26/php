CREATE TABLE persons(
  id SERIAL,
  firstName VARCHAR(100),
  lastName VARCHAR(100),
  age INT
);

INSERT INTO persons
  (firstName, lastName, age)
VALUES
 ('Иван', 'Иванов', 42);

INSERT INTO persons
  (firstName, lastName, age)
VALUES
 ('Пётр', 'Петров', 33);

INSERT INTO persons
  (firstName, lastName, age)
VALUES
 ('Екатерина', 'Сидорова', 19);

INSERT INTO persons
  (firstName, lastName, age)
VALUES
 ('Василий', 'Кузнецов', 24);

UPDATE persons
SET age=20
WHERE id=3;

DELETE FROM persons
WHERE id=4;

SELECT id, age, lastName
FROM persons;

SELECT id, lastName, age, 2020-age AS BirthYear
FROM persons
ORDER BY age DESC;

SELECT id, lastName, age, 2020-age AS BirthYear
FROM persons
ORDER BY age
LIMIT 1;

SELECT * FROM persons
WHERE age>20;

SELECT * FROM persons
WHERE age>20 AND lastName='Иванов';
