<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Заголовок</title>
</head>
<body>
    <a href="./../index.php">Назад</a>
    <article>
        <h2>
            <?php echo $this->data['article']['headline']; ?>
        </h2>
        <p>
            <?php echo $this->data['article']['text']; ?>
        </p>
        <p>
            <?php echo $this->data['article']['author']; ?>
        </p>
    </article>
    <hr>
    <form action="./../parts/update.php" method="post">
        <input type="hidden" name="id" value="<?php echo $this->data['article']['id']; ?>">
        <input type="submit" name="update" value="Изменить">
    </form>
    <form action="./../parts/send.php" method="post">
        <input type="hidden" name="id" value="<?php echo $this->data['article']['id']; ?>">
        <input type="submit" name="delete" value="Удалить">
    </form>
</body>
</html>
