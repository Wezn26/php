<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Новости</title>
</head>
<body>
    <h1>Главные новости</h1>
    <hr>
    <?php foreach ($this->data['news'] as $article): ?>
        <article>
            <!-- Заголовок -->
            <h2>
                <a href="./parts/article.php?id=<?php echo $article['id']; ?>">
                    <?php echo $article['headline']; ?>
                </a>
            </h2>
            <!-- Текст -->
            <p>
                <?php echo $article['text']; ?>
            </p>
            <!-- Автор -->
            <p>
                <?php echo $article['author'];?>
            </p>
        </article>
    <hr>
    <?php endforeach; ?>
    <h2>Добавить новость:</h2>
    <form action="./parts/send.php" method="post">
        Заголовок:<br>
        <input type="text" name="title"><br>
        Текст:<br>
        <textarea name="text" cols="30" rows="10"></textarea><br>
        Автор:<br>
        <input type="text" name="author"><br>
        <input type="submit" name="add" value="Добавить запись">
    </form>
</body>
</html>
