<?php

class DB
{
    protected $dbh;

    public function __construct()
    {
        $config = require __DIR__ . './../data/config.php';
        $this->dbh = new PDO($config['dsn'], $config['username'], $config['password']);
    }

    /*
     В PDO есть два типа запросов:
        1. Возвращающие результат (selest, show)
        1. Не возвращающие результат (insert, delete  и другие)
     */

    public function execute(string $sql, array $data = [])
    {
        $sth = $this->dbh->prepare($sql);
        $result = $sth->execute($data);
        if (false === $result) {
            return false;
        }
        return $result;
    }

    public function query(string $sql, array $data = [])
    {
        $sth = $this->dbh->prepare($sql);
        $result = $sth->execute($data);
        if (false === $result) {
            return false;
        }
        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }
}
