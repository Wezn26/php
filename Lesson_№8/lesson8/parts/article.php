<?php

require __DIR__ . './../classes/DB.php';
require __DIR__ . './../classes/View.php';

    if (isset($_GET['id'])) {
        $id = (int)$_GET['id'];
    } else {
        die('Новость не найдено!');
    }

    $db = new DB();

    $sql = 'SELECT * FROM news WHERE id=:id';
    $data = [
        ':id' => $id,
    ];

    $article = $db->query($sql, $data);

    $view = new View();
    $view->assign('article',array_shift($article));
    $view->display('article.php');


