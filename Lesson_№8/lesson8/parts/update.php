<?php
require __DIR__ . './../classes/DB.php';
require  __DIR__ . './../classes/View.php';

if (isset($_POST['id'])) {
    $id = (int)$_POST['id'];
}

$db = new DB();

$sql = 'SELECT * FROM news WHERE id=:id';
$data = [
    ':id' => $id,
];

$updateArticle = $db->query($sql, $data);

$view = new View();
$view->assign('updateArticle', array_shift($updateArticle));
$view->display('update.php');
