<?php
require_once __DIR__ . '/GuestBookRecord.php';
class GuestBook
{
  protected $path;
  protected $data = [];
  public function __construct()
  {
    $this->path = __DIR__.'/../db.txt';
    $records = file($this->path, FILE_IGNORE_NEW_LINES);
    foreach ($records as $record) {
      $this->data[] = new GuestBookRecord($record);
    }
  }

  public function getRecords()
  {
    return $this->data;
  }
  public function append(GuestBookRecord $record)
  {
    $this->data[] = $record;
  }
  public function save()
  {
    $records = [];
    foreach ($this->data as $record) {
      $records[] = $record->getMessage();
    }
    file_put_contents($this->path, implode("\n", $records));
  }
}
