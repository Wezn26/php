<?php
class A
{
  public function foo()
  {
    echo 'Foo!!!' . "<br />";
    return $this;
  }
  public function bar()
  {
    echo 'Bar!!!' . '<br />';
    return $this;
  }
}
$a = new A;
$a->foo()->bar();

echo gettype($a) . '<br />';
echo get_class($a);
