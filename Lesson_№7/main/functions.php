<?php
require_once __DIR__ . '/Classes/GuestBookRecord.php';

function getGuestBookRecords()
{
  $lines = file(__DIR__.'/db.txt', FILE_IGNORE_NEW_LINES);
  $ret = [];
  foreach ($lines as $line) {
    $ret[] = new GuestBookRecord($line);
  }
  return $ret;
}
