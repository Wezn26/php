<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Article</title>
  </head>
  <body>
    <?php
    $id = $_GET['id'];
    $article = $this->data['article'][$id];
     ?>
     <h1><?php echo $article->getArticle()['header']; ?></h1>
     <p><?php echo $article->getArticle()['text']; ?></p>
  </body>
</html>
