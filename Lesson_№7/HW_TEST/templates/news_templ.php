<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>HW_TEST_NEWS</title>
    <style>
      article {
        margin: 10px;
        padding: 10px;
        border: 1px solid red;
      }
    </style>
  </head>
  <body>
    <h1>HW_TEST_NEWS</h1>
    <br>
    <?php foreach ($this->data['news'] as $news): ?>
      <article>
        <h3>
          <a href="article.php?id=<?php echo $news->getArticle()['id']; ?>">
            <?php echo $news->getArticle()['header']; ?>
          </a>
          <p><?php echo $news->getArticle()['text']; ?></p>
        </h3>
      </article>
    <?php endforeach; ?>
  </body>
</html>
