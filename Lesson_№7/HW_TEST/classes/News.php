<?php
require __DIR__ . '/Article.php';
class News
{
  protected $news = [];
  public function __construct($path)
  {
    $articles = file($path, FILE_IGNORE_NEW_LINES);
    $i = 0;
    foreach ($articles as $article) {
      $this->news[++$i] = new Article($article);
    }
  }
  public function getNews()
  {
    return $this->news;
  }
}
// $news = new News(__DIR__.'/../db/news.txt');
// var_dump(
//   $news->getNews()[1],
//   $news->getNews()[2],
//   $news->getNews()[3]
// );
// var_dump(
//   $news->getNews()[1]->getArticle()['id'],
//   $news->getNews()[1]->getArticle()['header'],
//   $news->getNews()[1]->getArticle()['text']
// );
