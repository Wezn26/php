<?php

require __DIR__ . '/classes/News.php';
require __DIR__ . '/classes/View.php';

$news = new News(__DIR__ . '/db/news.txt');
$view = new View();
$view->assing('news', $news->getNews());
$view->display(__DIR__ . '/templates/news.html');