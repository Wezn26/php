<?php

class View
{
    protected $data = []; //данные

    /*
     * метод assing() сохраняет данные, передаваемые в шаблон по заданному имени
     */
    public function assing($name, $value)
    {
        $this->data[$name] = $value;
    }

    /*
     * метод display(), отображает указанный шаблон с заранее сохраненными данными
     */
    public function display($template)
    {
        include $template;
    }

    /*
     * метод render(), аналогичен методу display(), но не выводит шаблон, а возвращает;
     */
    public function render($template)
    {
        ob_start();
        include $template;
        ob_end_clean();
        return ob_get_contents();
    }
}