<?php
require __DIR__ . '/Article.php';

class News
{
    protected $news = []; //все новости

    public function __construct($path)
    {
        $data = file($path, FILE_IGNORE_NEW_LINES);
        $i = 0;
        foreach ($data as $articles) {
            $this->news[++$i] = new Article($articles);
        }
    }

    public function getNews() //получить все новости
    {
        return $this->news;
    }
}
