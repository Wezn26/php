<?php
require __DIR__ . '/Article.php';
class News
{
  protected $news = [];
  public function __construct($path)
  {
    $data = file($path, FILE_IGNORE_NEW_LINES);
    $i = 0;
    foreach ($data as $articles) {
      $this->news[++$i] = new Article($articles);
    }
  }
  public function getNews()
  {
    return $this->news;
  }
}
 //$news = new News(__DIR__.'./../db/news.txt');
 //var_dump($news->getNews());
