<?php
class Article
{
  protected $article = [];
  public function __construct($articles)
  {
    $data = explode("|", $articles);
    $this->article['id'] = $data[0];
    $this->article['header'] = $data[1];
    $this->article['text'] = $data[2];
  }
  public function getArticle()
  {
    return $this->article;
  }
}
 //$art = new Article('1|Header|Text');
 //var_dump($art->getArticle());
