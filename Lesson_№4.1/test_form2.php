<?php

$file = __DIR__ . '/form2.txt';
$file_array = file($file);

if (!empty($_POST['note'])) {
    $file_array[] = strip_tags(htmlspecialchars($_POST['note']));
    $result = implode("\n", $file_array);
    file_put_contents($file, $result);
}

?>

<html>
<head>
    <title>GUEST_BOOK</title>
</head>
<body>
  <?php foreach ($file_array as $file_string): ?>
     <div><p><?= $file_string ?></p></div>
  <?php endforeach; ?>

<form action="test_form2.php" method="POST">
    <input type="text" name="note">
    <input type="submit" value="Отправить">
</form>
</body>
</html>
