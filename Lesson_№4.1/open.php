<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>OPEN_FILES</title>
  </head>
  <body>
<?php
# 1
   // $res = fopen(__DIR__ . '/data.txt', 'r');
   //
   // $str = fgets($res);
   // echo $str;
   //
   // $str = fgets($res);
   // echo $str;

# 2
//  $res = fopen(__DIR__ . '/data.txt', 'r');
//
// while (!feof($res)) {
//   $str = fgets($res);
//   echo $str;
// }
// fclose($res);

# 3
// $file = file(__DIR__ . '/data.txt');
// var_dump($file);

# 4
// $file = file_get_contents(__DIR__ . '/data.txt');
// echo $file;

# 5
// readfile(__DIR__ . '/data.txt');

# 6
// var_dump(file_exists(__DIR__ . '/data.txt'));

# 7
// var_dump(is_readable(__DIR__ . '/data.txt'));

// ЗАПИСЬ В ФАЙЛ

# 1
// $file = fopen(__DIR__ . '/data.txt', 'w');
// $data = 'В лесу родилась ёлочка';
// fwrite($file, $data);
// fclose($file);

# 2
// $path = __DIR__ . '/data.txt';
// $data = 'В лесу родилась ёлочка,
// В лесу она росла,
// Зимой и летом стройная,
// Зелёная была ...';
// file_put_contents($path, $data);

# 3



 ?>
  </body>
</html>
