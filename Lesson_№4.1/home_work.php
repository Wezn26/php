<?php require __DIR__ . '/upload.php' ?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>HOME_WORK</title>
  </head>
  <body>
<?php
// 1. Создайте текстовый файл, в котором будут находиться записи гостевой. Одна строка - одна запись. Заполните файл 2-3 записями.
// $file = fopen(__DIR__ . '/home.txt', 'a');
// $data = 'Первая строка!!!' . "\n";
// fwrite($file, $data);

// $data1 = 'Вторая строка' . "\n";
// fwrite($file, $data1);

// $data1 = 'Третья строка' . "\n";
// fwrite($file, $data1);

// 2. Напишите функцию, которая будет читать этот файл и возвращать массив записей гостевой книги
$file = file(__DIR__ . '/home.txt');
# 1__ й способ
// foreach ($file as $value) {
//   echo $value . "<br />";
// }
# 2__й способ
// $str = implode("<br />", $file);
// echo $str;
// var_dump($file);
  ?>

  <form action="home_work.php" method="post">
    <input type="text" name="text" placeholder="Введите текст сюда">
    <button type="submit" name="button">Отправить</button>
  </form>
  </body>
</html>
