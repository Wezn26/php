<?php
    require __DIR__ . '/classes/GuestBook.php';
    $guestBook = new GuestBook(__DIR__ . '/data/db.txt');
?>
<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Гостевая книга</title>
</head>
<body>
<h1>Записи:</h1>
<hr>
<hr>
<?php foreach ($guestBook->getData() as $record):?>
<p><?php echo $record; ?></p>
<hr>
<?php endforeach; ?>
<hr>
<form action="/script.php" method="post">
    Добавить запись:
    <input type="text" name="message">
    <button type="submit">Отправить</button>
</form>
</body>
</html>
