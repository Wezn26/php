<?php
require __DIR__ . '/TextFile.php';

class GuestBook extends TextFile
{
    protected $pathToFile;

    public function __construct($pathToFile) //передается путь до файла с данными гостевой книги
    {
        $this->pathToFile = $pathToFile;
        $this->data = file($this->pathToFile, FILE_IGNORE_NEW_LINES);
    }

    public function append($record) //добавлдяет новую запись гостевой книги
    {
        $this->data[] = $record;
        return $this;
    }

    public function save() //сохраняет массив в файл
    {
        file_put_contents($this->pathToFile, implode("\n", $this->data));
    }
}