<?php

class Uploader
{
    protected $field;

    public function __construct($field)
    {
        $this->field = $field;
    }

    public function isUploaded()
    {
        return is_uploaded_file($this->field['tmp_name']);
    }

    public function upload()
    {
        move_uploaded_file($this->field['tmp_name'],
            __DIR__ . '/../images/'.$this->field['name']);
    }
}