<?php
require __DIR__ . '/../classes/Uploader.php';

if (!empty($_FILES['image'])) {
    $field = $_FILES['image'];
    $upload = new Uploader($field);
    if ($field['error'] == 0) {
        if ($upload->isUploaded()) {
            $upload->upload();
        }
    }
}

header('Location: /lesson6/upload/');
exit;