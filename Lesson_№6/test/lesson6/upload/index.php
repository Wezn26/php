<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Загрузка на сервер</title>
</head>
<body>
<form action="test/lesson6/upload/upload.php" method="post" enctype="multipart/form-data">
    Загрузите картинку на сервер:
    <input type="file" name="image">
    <button type="submit">Отправить</button>
</form>
</body>
</html>
