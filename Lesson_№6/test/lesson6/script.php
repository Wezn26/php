<?php
require __DIR__ . '/classes/GuestBook.php';

if (isset($_POST['message'])) {
    $guestBook = new GuestBook(__DIR__ . '/data/db.txt');
    $guestBook->append($_POST['message'])->save();
}

header('Location: /lesson6/');
exit;