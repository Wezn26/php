<?php
  class Item
  {
    public $color;
    protected $price;

    public function setPrice($price)
    {
      if ($price <= 0) {
        echo "ERROR!!!";
        die;
      }
      $this->price = $price;
    }

    public function show()
    {
      return 'Я стою ' . $this->price . ' руб ' . '<br />';
    }
  }
