<?php
require __DIR__ . '/Classes/Table.php';
require __DIR__ . '/Classes/Cabinet.php';
?>
<!DOCTYPE html>
<html lang="ru" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>CLASSES</title>
  </head>
  <body>
<?php
  // $table = new Table;
  // $table->color = 'red';
  // $table->legs = 4;
  // $table->price = 1000;
  // var_dump($table);
  // echo $table->show();

  $table1 = new Table;
  $table1->setPrice(1000);

  echo $table1->show();

  $table2 = new Table;
  $table2->setPrice(2000);

  echo $table2->show();

  $cab = new Cabinet;
  $cab->doors = 2;
  $cab->setPrice(5000);

  echo $cab->show();

  // var_dump($cab);

 ?>
  </body>
</html>
