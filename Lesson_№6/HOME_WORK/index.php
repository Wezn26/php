<?php
  require __DIR__ . '/Classes/GuestBook.php';
  require __DIR__ . '/Classes/Uploader.php';
  $guestBook = new GuestBook(__DIR__ . '/db.txt', 'note');
  $file = new Uploader('upload-file');
 ?>
<!DOCTYPE html>
<html lang="ru" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>OOП.ГОСТЕВАЯ КНИГА</title>
  </head>
  <body>
<h1 style="text-align: center">Lesson 5. ГОСТЕВАЯ КНИГА</h1>
<!-- NOTES START -->
<h2>GUEST BOOK</h2>
<form action="#" method="post">
    <input type="text" name="note" value="<?php if (!empty($_POST['note'])) echo $_POST['note']  ?>">
    <button type="submit" style="cursor: pointer;">ОПРАВИТЬ</button>
</form>
<h3>ЗАПИСИ В ГОСТЕВОЙ КНИГЕ:</h3>
<?php $guestBook->upload() ?>

<?php foreach ($guestBook->getData() as $file_string): ?>
   <div><p><?= $file_string ?></p></div>
<?php endforeach; ?>
<!-- NOTES END -->
<hr>
<!-- FILE UPLOAD START -->
<h2>File Upload</h2>
<p>Выбирите файл</p>
<form action="#" method="post" enctype="multipart/form-data">
  <input type="file" name="upload-file">
  <button type="submit" name="button">Отправить</button>
</form>
<!-- FILE UPLOAD END -->
<?php echo ($file->upload()) ? 'Файл загружен!!!' : 'Файл не загружен!!!' ?>
  </body>
</html>
