<?php

class Uploader
{
  protected $field_form_name;

  public function __construct($field_form_name)
  {
    $this->field_form_name = $field_form_name;
  }

  protected function isUploaded()
  {
    return (!empty($_FILES) && !empty($_FILES[$this->field_form_name])) ? true : false;
  }

  public function upload()
  {
    if ($this->isUploaded()) {
      $file = $_FILES[$this->field_form_name];
      if ($file['error'] == 0) {
        move_uploaded_file($file['tmp_name'], __DIR__ . './../img/' . $file['name']);
        return true;
      }
    } else {
      return false;
    }
  }
}
