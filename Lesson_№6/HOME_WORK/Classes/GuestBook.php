<?php

class GuestBook
{
    protected $field_form_name;
    protected $file_address;
    protected $data;

    public function __construct($file_address, $field_form_name)
    {
      $this->file_address = $file_address;
      $this->field_form_name = $field_form_name;
      $this->data = file($this->file_address);
    }

    public function getData()
    {
      return $this->data;
    }

    public function isUploaded()
    {
      return (!empty($_POST[$this->field_form_name])) ? true : false;
    }

    public function putData()
    {
      if ($this->isUploaded()) {
        $note = $_POST[$this->field_form_name];
        $this->data[] = "\n" . $note;
        return true;
      } else {
        return false;
      }
    }

    public function save()
    {
      file_put_contents($this->file_address, $this->data);
    }

    public function upload()
    {
      if ($this->putData()) {
        $this->save();
      }
    }
}
