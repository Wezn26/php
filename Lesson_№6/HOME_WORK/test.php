<?php
class GuestBook
{
  protected $field_form_name;
  protected $file_address;
  protected $data;

  public function __construct($file_address, $field_form_name)
  {
    $this->file_address = $file_address;
    $this->field_form_name = $field_form_name;
    $this->data = file($this->file_address);
  }

  public function getData()
  {
    return $this->data;
  }

  public function isUploaded()
  {
    return (!empty($_POST[$this->field_form_name])) ? true : false;
  }

  public function putData()
  {
    if ($this->isUploaded()) {
      $note = $_POST[$this->field_form_name];
      $this->data[] = "\n" . $note;
      return true;
    } else {
      return false;
    }
  }

  public function save()
  {
    file_put_contents($this->file_address, $this->data);
  }

  public function upload()
  {
    if ($this->putData()) {
      $this->save();
    }
  }
}

class Uploader
{
  protected $field_form_name;

  public function __construct($field_form_name)
  {
    $this->field_form_name = $field_form_name;
  }

  protected function isUploaded()
  {
    return (!empty($_FILES) && !empty($_FILES[$this->field_form_name])) ? true : false;
  }

  public function upload()
  {
    if ($this->isUploaded()) {
      $file = $_FILES[$this->field_form_name];
      if ($file['error'] == 0) {
        move_uploaded_file($file['tmp_name'], __DIR__ . '/img/' . $file['name']);
        return true;
      }
    } else {
      return false;
    }
  }
}

$guestBook = new GuestBook(__DIR__.'/db2.txt', 'note');
$file = new Uploader('file');
 ?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>TEST_GUEST_BOOK</title>
  </head>
  <body>
<h1 style="text-align: center">NOTES_GUEST_BOOK</h1>
<!-- NOTES START -->
<h2>GUEST_BOOK</h2>
<form action="#" method="post">
  <input type="text" name="note" value="<?php if (!empty($_POST['note']))  echo $_POST['note']  ?>">
  <button type="submit" name="button">Send</button>
</form>
<h3>VIEW_RECORDS</h3>
<?php $guestBook->upload() ?>
<?php foreach ($guestBook->getData() as $file_string): ?>
  <div><p><?= $file_string ?></p></div>
<?php endforeach; ?>
<!-- NOTES END -->
<hr>
<!-- FILE UPLOAD START -->
<h2>FILE_UPLOAD</h2>
<p>Choose File</p>
<form action="#" method="post" enctype="multipart/form-data">
  <input type="file" name="file">
  <button type="submit" name="button">Send</button>
</form>

<?php echo($file->upload()) ? 'File downloaded!!!' : 'File not downloaded!!!'  ?>
<!-- FILE UPLOAD END -->
  </body>
</html>
